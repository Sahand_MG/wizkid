<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\WizkidController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('user/')->group(function() {
    Route::get('add', [WizkidController::class, 'getAddWizkid'])->name('add');
    Route::post('add', [WizkidController::class, 'postAddWizkid'])->name('add');
    Route::get('view', [WizkidController::class, 'viewWizkid'])->name('index');
    Route::get('edit/{idx}', [WizkidController::class, 'getEditWizkid'])->name('edit');
    Route::post('edit/{idx}', [WizkidController::class, 'postEditWizkid'])->name('edit');
    Route::get('delete/{idx}', [WizkidController::class, 'deleteWizkid'])->name('delete');
    Route::get('login', [AuthController::class, 'getLogin'])->name('login');
    Route::post('login', [AuthController::class, 'postLogin'])->name('login');
    Route::get('logout', [AuthController::class, 'postLogout'])->name('logout');
    Route::get('search', [WizkidController::class, 'search'])->name('search');
    Route::middleware('auth')->group(function() {
        Route::get('fire/{idx}', [WizkidController::class, 'fireWizkid'])->name('fire');
        Route::get('unfire/{idx}', [WizkidController::class, 'unfireWizkid'])->name('unfire');
    });
});