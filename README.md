## Instrauctions

- copy .env.example to .env
- composer install
- php artisan key:generate
- php artisan migrate
- php artisan queue:work
- queue driver is set to database
- a cronjob is set for clearing fired wizkids after a week
- for checking email notification, mailtrap driver is used