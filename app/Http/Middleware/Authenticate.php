<?php

namespace App\Http\Middleware;

use App\Consts\RequestConst;
use App\Services\Response\Responder;
use App\Services\Response\UnAuthorizedResponse;
use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Tymon\JWTAuth\Facades\JWTAuth;


class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    protected function redirectTo($request)
    {
        if (! $request->expectsJson()) {
            return route('login');
        } else {
            return response()->json(Responder::render(new UnAuthorizedResponse(), RequestConst::AUTH_RESPONSE));
        }
    }
}
