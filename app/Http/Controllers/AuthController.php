<?php

namespace App\Http\Controllers;

use App\Consts\RequestConst;
use App\Http\Controllers\Actions\UserLoginAction;
use App\Layers\Validation\UserLoginValidation;
use App\Services\Response\OkResponse;
use App\Services\Response\Responder;
use App\Services\SideBar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{

    public function getLogin()
    {
        $sidebar_items = SideBar::getItems();
        return view('login', compact('sidebar_items'));
    }

    public function postLogin()
    {
        $validatedData = UserLoginValidation::install(\request()->all());
        (new UserLoginAction())->execute($validatedData);
        return redirect()->route('index');
    }

    public function postLogout()
    {
        Auth::logout();
        return redirect()->route('index');
    }
}
