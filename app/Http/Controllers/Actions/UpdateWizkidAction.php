<?php


namespace App\Http\Controllers\Actions;


use App\Repositories\UserDB;
use App\Services\EloquentDataShaper;

class UpdateWizkidAction
{
    public function execute($userData)
    {
        $id = hashid($userData['idx']);
        unset($userData['idx']);
        $u = UserDB::updateUser($id, $userData);
        return [
            'user' => EloquentDataShaper::shape($u, userModelHiddenFields(), 'except')
        ];
    }
}