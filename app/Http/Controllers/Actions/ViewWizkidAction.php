<?php


namespace App\Http\Controllers\Actions;


use App\Repositories\UserDB;
use App\Services\EloquentDataShaper;

class ViewWizkidAction
{
    public function execute()
    {
        $usersCollection = UserDB::getAllUsers();
        $users = EloquentDataShaper::groupShape($usersCollection, userModelHiddenFields(), 'except');
        return ['users' => $users];
    }
}