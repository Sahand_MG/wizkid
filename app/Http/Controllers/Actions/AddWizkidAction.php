<?php

namespace App\Http\Controllers\Actions;


use App\Repositories\UserDB;
use App\Services\EloquentDataShaper;
use Illuminate\Support\Str;

class AddWizkidAction
{
    public function execute(array $wizkidData)
    {
        $file = request()->file('picture');
        $newFileName = Str::random(10).'.'.$file->getClientOriginalExtension();
        $path = storage_path('app/public/users/images');
        $file->move($path, $newFileName);
        $wizkidData['picture'] = $newFileName;
        $user = UserDB::addUser($wizkidData);
        return ['user' => EloquentDataShaper::shape($user, userModelHiddenFields(),'except')];
    }
}