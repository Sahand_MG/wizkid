<?php


namespace App\Http\Controllers\Actions;


use App\Exceptions\InvalidCredsException;
use App\Services\EloquentDataShaper;
use Illuminate\Support\Facades\Auth;

class UserLoginAction
{
    public function execute($data)
    {
        if (auth()->attempt($data)) {
            return ['user' => EloquentDataShaper::shape(Auth::user(), userModelHiddenFields(), 'except')];
        } else {
            throw new InvalidCredsException('Invalid Credentials');
        }
    }
}