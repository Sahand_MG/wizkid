<?php


namespace App\Http\Controllers\Actions;


use App\Exceptions\SelfFireException;
use App\Jobs\SendFireEmailJob;
use App\Repositories\UserDB;
use Illuminate\Support\Facades\Auth;

class FireWizkidAction
{
    public function execute($idx)
    {
        $id = hashid($idx);
        if (Auth::id() == $id) {
            throw new SelfFireException('Can\'t fire your self!');
        }
        $user = UserDB::softDeleteUser($id);
        SendFireEmailJob::dispatch($user, 'fired');
    }
}