<?php


namespace App\Http\Controllers\Actions;


use App\Repositories\UserDB;
use App\Services\EloquentDataShaper;

class DeleteWizkidAction
{
    public function execute($idx)
    {
        $id = hashid($idx);
        $u = UserDB::hardDeleteUser($id);
        return [
            'user' => EloquentDataShaper::shape($u, userModelHiddenFields(), 'except')
        ];
    }
}