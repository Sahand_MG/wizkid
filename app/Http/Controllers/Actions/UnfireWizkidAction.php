<?php


namespace App\Http\Controllers\Actions;


use App\Exceptions\SelfFireException;
use App\Jobs\SendFireEmailJob;
use App\Repositories\UserDB;
use Illuminate\Support\Facades\Auth;

class UnfireWizkidAction
{
    public function execute($idx)
    {
        $id = hashid($idx);
        if (Auth::id() == $id) {
            throw new SelfFireException('Can\'t fire/unfire your self!');
        }
        $user = UserDB::removeSoftDelete($id);
        SendFireEmailJob::dispatch($user, 'unfired');
    }
}