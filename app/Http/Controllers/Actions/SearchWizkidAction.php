<?php


namespace App\Http\Controllers\Actions;


use App\Repositories\UserDB;
use App\Services\EloquentDataShaper;

class SearchWizkidAction
{
    public function execute($data)
    {
        $nullableUsers = UserDB::getUsersByRoleOrName($data['role'], $data['name']);
        $users = $nullableUsers->getOr();
        $users = EloquentDataShaper::groupShape($users, userModelHiddenFields(), 'except');
        return ['users' => $users];
    }
}