<?php

namespace App\Http\Controllers;

use App\Consts\RequestConst;
use App\Http\Controllers\Actions\AddWizkidAction;
use App\Http\Controllers\Actions\DeleteWizkidAction;
use App\Http\Controllers\Actions\FireWizkidAction;
use App\Http\Controllers\Actions\SearchWizkidAction;
use App\Http\Controllers\Actions\UnfireWizkidAction;
use App\Http\Controllers\Actions\UpdateWizkidAction;
use App\Http\Controllers\Actions\ViewWizkidAction;
use App\Layers\Validation\AddWizkidValidation;
use App\Layers\Validation\SearchWizkidValidation;
use App\Layers\Validation\UpdateWizkidValidation;
use App\Repositories\UserDB;
use App\Services\SideBar;


class WizkidController extends Controller
{
    public function getAddWizkid()
    {
        $sidebar_items = SideBar::getItems();
        return view('add-wizkid', compact('sidebar_items'));
    }

    public function postAddWizkid()
    {
        $validatedData = AddWizkidValidation::install(\request()->all());
        $resp = (new AddWizkidAction())->execute($validatedData);
        return redirect()->route('index');
    }

    public function viewWizkid()
    {
        $resp = (new ViewWizkidAction())->execute();
        $resp['sidebar_items'] = SideBar::getItems();
        return view('view', $resp);
    }

    public function getEditWizkid($idx)
    {
        $sidebar_items = SideBar::getItems();
        $id = hashid($idx);
        $nullableUser = UserDB::getUserById($id);
        $user = $nullableUser->getOr();
        return view('update-wizkid', compact('sidebar_items', 'user'));
    }

// Only name update is considered
    public function postEditWizkid($idx)
    {
        $validatedData = UpdateWizkidValidation::install(\request()->all());
        $validatedData['idx'] = $idx;
        (new UpdateWizkidAction())->execute($validatedData);
        return redirect()->route('index')->with(['message' => 'Record Edited']);
    }

    public function deleteWizkid($idx)
    {
        (new DeleteWizkidAction())->execute($idx);
        return redirect()->route('index')->with(['message' => 'Record Deleted']);
    }

    public function fireWizkid($idx)
    {
        (new FireWizkidAction())->execute($idx);
        return redirect()->back()->with(['message' => 'Wizkid Fired']);
    }

    public function unfireWizkid($idx)
    {
        (new UnfireWizkidAction())->execute($idx);
        return redirect()->back()->with(['message' => 'Wizkid Unfired']);
    }

    public function search()
    {
        $validatedData = SearchWizkidValidation::install(\request()->all());
        $resp = (new SearchWizkidAction())->execute($validatedData);
        $resp['sidebar_items'] = SideBar::getItems();
        return view('view', $resp);
    }
}
