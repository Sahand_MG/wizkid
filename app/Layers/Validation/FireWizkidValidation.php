<?php


namespace App\Layers\Validation;


use App\Exceptions\WizkidException;
use Illuminate\Support\Facades\Validator;

class FireWizkidValidation
{
    public static function install($params)
    {
        $v = Validator::make($params, [
            'idx' => 'required|string',
        ]);
        if ($v->fails()) {
            throw new WizkidException($v->getMessageBag()->first());
        }
        return $v->validated();
    }
}