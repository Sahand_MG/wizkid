<?php


namespace App\Layers\Validation;


use App\Exceptions\WizkidException;
use Illuminate\Support\Facades\Validator;

class UpdateWizkidValidation
{
    public static function install($params)
    {
        $v = Validator::make($params, [
            'name' => 'sometimes|required|string|min:3|max:255',
        ]);
        if ($v->fails()) {
            throw new WizkidException($v->getMessageBag()->first());
        }
        return $v->validated();
    }
}