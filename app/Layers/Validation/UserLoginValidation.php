<?php


namespace App\Layers\Validation;


use App\Exceptions\WizkidException;
use Illuminate\Support\Facades\Validator;

class UserLoginValidation
{
    public static function install($params)
    {
        $v = Validator::make($params, [
            'email' => 'required|email|max:255',
            "password" => 'required|string|min:6'
        ]);
        if ($v->fails()) {
            throw new WizkidException($v->getMessageBag()->first());
        }
        return $v->validated();
    }
}