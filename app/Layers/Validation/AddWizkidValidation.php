<?php


namespace App\Layers\Validation;


use App\Exceptions\WizkidException;
use Illuminate\Support\Facades\Validator;

class AddWizkidValidation
{
    public static function install($params)
    {
        $params = array_filter($params);
        $v = Validator::make($params, [
            'name' => 'required|string|min:3|max:255',
            'role' => 'required|string',
            'email' => 'required|email|unique:users|max:255',
            'picture' => 'required|mimes:png,jpg,jpeg|max:1024',
            'phone' => 'sometimes|regex:/(31)[0-9]{9}/',
            "password" => 'required|string|min:6'
        ]);
        if ($v->fails()) {
            throw new WizkidException($v->getMessageBag()->first());
        }
        return $v->validated();
    }
}