<?php


use App\Exceptions\InvalidHashException;
use Illuminate\Support\Facades\Auth;
use Vinkla\Hashids\Facades\Hashids;

if (!function_exists('user')) {
    function user() {
        return Auth::user();
    }
}

function userModelHiddenFields() {
    if (Auth::check()) {
        return ['created_at', 'updated_at', 'email_verified_at'];
    }
    return ['created_at', 'updated_at', 'deleted_at', 'email_verified_at', 'email', 'phone'];
}

if (!function_exists('hashid')) {
    function hashid($data) {
        if (gettype($data) == 'string') {
            return Hashids::decode($data);
        } elseif(gettype($data) == 'integer'){
            return Hashids::encode($data);
        } else {
            throw new InvalidHashException('Invalid hash');
        }
    }
}