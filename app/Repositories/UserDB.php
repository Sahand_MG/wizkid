<?php


namespace App\Repositories;


use App\Exceptions\DataNotFoundException;
use App\Models\User;
use App\Services\Nullable;
use Carbon\Carbon;

class UserDB
{
    public static function addUser($userData)
    {
        return User::query()->create($userData);
    }

    public static function getAllUsers($paginate = 50)
    {
        return User::query()->withTrashed()->paginate($paginate)->items();
    }

    public static function updateUser($id, $data)
    {
        $user = User::query()->where('id', $id)->firstOr(function () {
            throw new DataNotFoundException('Invalid Wizkid');
        });
        $user->update($data);
        return $user;
    }

    public static function softDeleteUser($id)
    {
        $user = User::query()->where('id', $id)->firstOr(function () {
            throw new DataNotFoundException('Invalid Wizkid');
        });
        $user->delete($user);
        return $user;
    }

    public static function hardDeleteUser($id)
    {
        $user = User::query()->where('id', $id)->firstOr(function () {
            throw new DataNotFoundException('Invalid Wizkid');
        });
        $basePath = public_path('storage/users/images/');
        $img = $user->getAttributes()['picture'];
        if (file_exists($basePath.$img)) {
            unlink($basePath.$img);
        }
        $user->forceDelete($user);
        return $user;
    }

    public static function removeSoftDelete($id)
    {
        $user = User::query()->withTrashed()->where('id', $id)->firstOr(function () {
            throw new DataNotFoundException('Invalid Wizkid');
        });
        $user->update(['deleted_at' => null]);
        return $user;
    }

    public static function hardDeleteAllFiredUsers()
    {
        User::query()->where('deleted_at', '<', Carbon::now()->subWeek())->forceDelete();
    }

    public static function getUserById($id) :Nullable
    {
        return new Nullable(User::query()->where('id', $id)->first());
    }

    public static function getUsersByRoleOrName($role, $name)
    {
        return new Nullable(User::query()->where('role', $role)->where('name', 'like',"%$name%")->get());
    }
}