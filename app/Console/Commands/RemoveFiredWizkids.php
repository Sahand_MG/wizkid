<?php

namespace App\Console\Commands;

use App\Repositories\UserDB;
use Illuminate\Console\Command;

class RemoveFiredWizkids extends Command
{

    protected $signature = 'wizkid:remove';

    protected $description = 'removes fired wizkid after a week';

    public function handle()
    {
        UserDB::hardDeleteAllFiredUsers();
    }
}
