<?php

namespace App\Exceptions;

use App\Consts\RequestConst;
use App\Services\Response\Responder;
use App\Services\Response\UnAuthorizedResponse;
use Exception;

class InvalidCredsException extends Exception
{
    public function render()
    {
        return redirect()->back()->with(['error' => $this->getMessage()]);
    }
}
