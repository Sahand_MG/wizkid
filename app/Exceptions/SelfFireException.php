<?php

namespace App\Exceptions;

use App\Consts\RequestConst;
use App\Services\Response\BadRequestResponse;
use App\Services\Response\NotFoundResponse;
use App\Services\Response\Responder;
use Exception;

class SelfFireException extends Exception
{
    public function render()
    {
        return redirect()->back()->with(['error' => $this->getMessage()]);
    }
}
