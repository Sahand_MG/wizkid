<?php


namespace App\Services;


use App\Exceptions\DataNotFoundException;

class Nullable
{
    private $hasError;
    private $data;

    public function __construct($data)
    {
        if (gettype($data) == 'NULL') {
            $this->hasError = true;
        } elseif (gettype($data) == 'array') {
            if (count($data) == 0) {
                $this->hasError = true;
            } else {
                $this->data = $data;
            }
        } else {
            $this->data = $data;
        }
    }

    public function getOr()
    {
        if ($this->hasError) {
            throw new DataNotFoundException('Data Not Found');
        } else {
            return $this->data;
        }
    }
}