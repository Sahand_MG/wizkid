<?php


namespace App\Services\Response;


use App\Consts\RequestConst;

class NotFoundResponse extends AbstractResponse
{

    public function make($type, $data, $token, $state)
    {
        $this->payload = $data;
        $this->type = $type;
        $this->state = is_null($state) ? RequestConst::OK_TEXT : $state;
        $this->token = $token;
        $this->status = RequestConst::NOT_FOUND;
        return $this->getResponse();
    }
}