<?php


namespace App\Services\Response;


use App\Consts\RequestConst;

class BadRequestResponse extends AbstractResponse
{

    public function make($type, $data, $token, $state)
    {
        $this->payload = $data;
        $this->type = $type;
        $this->state = is_null($state) ? RequestConst::BAD_REQUEST_TEXT : $state;
        $this->token = $token;
        $this->status = RequestConst::BAD_REQUEST;
        return $this->getResponse();
    }
}