<?php


namespace App\Services\Response;


abstract class AbstractResponse
{
    protected $type;
    protected $status;
    protected $state;
    protected $payload;
    protected $token;

    abstract public function make($type, $data, $token, $state);

    public function getResponse()
    {
        $data = [
            'type' => $this->type,
            'status' => $this->status,
            'state' => $this->state,
            'payload' => $this->payload,
            'token' => $this->token,
        ];
        return response()->json($data, $this->status);
    }
}