<?php


namespace App\Services\Response;


use App\Consts\RequestConst;

class UnAuthorizedResponse extends AbstractResponse
{

    public function make($type, $data, $token, $state)
    {
        $this->payload = $data;
        $this->type = $type;
        $this->state = is_null($state) ? RequestConst::UN_AUTHORIZED_TEXT : $state;
        $this->token = $token;
        $this->status = RequestConst::UN_AUTHORIZED;
        return $this->getResponse();
    }
}