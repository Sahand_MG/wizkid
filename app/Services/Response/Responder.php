<?php


namespace App\Services\Response;


class Responder
{
    public static function render(AbstractResponse $abstractResponse, $response_type, array $data = [], $token = '', $state = null)
    {
        return $abstractResponse->make($response_type, $data, $token, $state);
    }
}