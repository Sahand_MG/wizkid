<?php


namespace App\Services;


use Illuminate\Support\Facades\Auth;

class SideBar
{
    public static function getItems()
    {
        $current_route = request()->route();
        $current_route_name = $current_route->action['as'];
        return [
            'wizkids' => [
                'show' => true,
                'en' => 'wizkids',
                'icon' => 'fa fa-users nav-icon',
                'href' => route('index'),
                'title' => 'Wizkids',
                'is_active' => str_contains($current_route_name, 'index'),
            ],
            'login' => [
                'show' => !Auth::check(),
                'en' => 'login',
                'icon' => 'nav-icon fa fa-sign-in',
                'href' => route('login'),
                'title' => 'login',
                'is_active' => str_contains($current_route_name, 'login'),
            ],

            'logout' => [
                'show' => Auth::check(),
                'en' => 'logout',
                'icon' => 'nav-icon fa fa-sign-out',
                'href' => route('logout'),
                'title' => 'logout',
                'is_active' => str_contains($current_route_name, 'logout'),
            ],
        ];
    }
}
