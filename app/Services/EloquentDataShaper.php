<?php


namespace App\Services;


class EloquentDataShaper
{
    public static function shape($eloquent, array $keys, $type = 'only')
    {
        $data = collect($eloquent);
        if ($type != 'only') {
            foreach ($data as $index => $record) {
                for ($t = 0; $t < count($keys); $t++) {
                    unset($data[$keys[$t]]);
                }
            }
            return $data->toArray();
        } else {
            return $data->only($keys)->toArray();
        }
    }

    public static function groupShape($data, array $keys, $type = 'only')
    {
        if ($type == 'only') {

            foreach ($data as $index => $record) {
                $data[$index] = $record->only($keys);
            }

        } else {
            foreach ($data as $index => $record) {
                for ($t = 0; $t < count($keys); $t++) {
                    unset($record[$keys[$t]]);
                }
                $data[$index] = $record;
            }
        }
        return $data;
    }
}