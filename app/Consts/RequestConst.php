<?php


namespace App\Consts;


use Illuminate\Http\Response;

class RequestConst
{
    // Response Types
    const AUTH_RESPONSE = 'Auth_Response';
    const WIZKID_RESPONSE = 'Wizkid_Response';

    // Response Status
    const OK = Response::HTTP_OK;
    const UN_AUTHORIZED = Response::HTTP_UNAUTHORIZED;
    const BAD_REQUEST = Response::HTTP_BAD_REQUEST;
    const NOT_FOUND = Response::HTTP_NOT_FOUND;


    // Response Texts
    const OK_TEXT = 'Ok';
    const UN_AUTHORIZED_TEXT = 'unAuthorized';
    const BAD_REQUEST_TEXT = 'badRequest';
    const NOT_FOUND_TEXT = 'notFound';
}