<?php


namespace App\Consts;


class PermissionConst
{
    const MANAGE = 'manage';
    const CREATE_USER = 'create_user';
    const DELETE_USER = 'delete_user';
    const UPDATE_USER = 'update_user';
    const VIEW_USERS = 'read_user';
    const VIEW_USER_DETAILS = 'read_user_details';
}