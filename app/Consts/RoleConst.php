<?php


namespace App\Consts;


class RoleConst
{
    const BOSS = 'boss';
    const DEVELOPER = 'developer';
    const DESIGNER = 'designer';
    const INTERN = 'intern';
}