<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendFireEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $user;
    public $state;

    public function __construct($user, $state = 'fired')
    {
        $this->user = $user;
        $this->state = $state;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $data = ['name' => $this->user->name, 'state' => $this->state];
        Mail::send('mail.fire-email', $data, function($msg) {
            $msg->to($this->user->email);
            $msg->from('info@owow.io');
            $msg->subject('New State!');
        });
    }
}
