function setNuid(event,usernames) {
    document.querySelector('#nuid').value = event.target.innerHTML;
    var users_container = document.querySelector('#users');
    users_container.value = '';
    for(var i = 0; i<usernames.length; i++){
        users_container.value += usernames[i]+'\n';
    }
}
function search(event) {
    var searched = event.target.value;
    var nuids = document.querySelectorAll("p[id='nuid-container']");
    nuids.forEach(function(nuid,index){
        var div_el = nuid.parentElement;
        if(nuid.innerHTML.indexOf(searched) > -1){
            div_el.style.display = '';
        }else{
            div_el.style.display = 'none';
        }
    })
}
var ut = 1;
var se = 1;
function getLogs(uint = ut, sensor = se, type = 'both') {
    const nuid = document.querySelector('#nuid').value;
    if(!nuid){
        createAlert('','','شناسه شبکه انتخاب شود','danger',true,true,'pageMessages');
    }else{
        axios.post(networkUrl+"?nuid="+nuid+'&ut='+uint+'&se='+sensor).then(function (response) {
            if(response.data[0].length == 0 && response.data[1].length == 0){
                createAlert('','','لاگی برای این شبکه یافت نشد','danger',true,true,'pageMessages');
                checkUnitMoreBtnStatus();
                checkSensorsMoreBtnStatus();
            }else {
                switch (type) {
                    case 'unit':
                        fillUnitsTable(response.data[0]);
                        break;
                    case 'sensor':
                        fillSensorsTable(response.data[1]);
                        break;
                    default:
                        document.querySelector('#tbody-units').innerHTML = '';
                        document.querySelector('#tbody-sensors').innerHTML = '';
                        fillUnitsTable(response.data[0]);
                        fillSensorsTable(response.data[1]);
                        break;
                }
            }
        }).catch(function(error){
            console.log(error);
            createAlert('','','error','danger',true,true,'pageMessages');

        });
    }
}

function fillUnitsTable(units) {
    const tbody = document.querySelector('#tbody-units');
    var temp;
    tbody.insertAdjacentHTML('beforeend',units);
    checkUnitMoreBtnStatus(units);
}

function checkUnitMoreBtnStatus(units = []) {
    let btn = document.querySelector('.more-units-btn-container button');
    if (btn.classList.contains('d-none')){
        btn.classList.toggle('d-none')
    }
    if(units.length == 0){
        btn.classList.toggle('d-none')
    }
}

function  fillSensorsTable(sensors) {
    const tbody = document.querySelector('#tbody-sensors');
    var temp;
    tbody.insertAdjacentHTML('beforeend',sensors);
    checkSensorsMoreBtnStatus(sensors);
}

function checkSensorsMoreBtnStatus(sensors = []) {
    let btn = document.querySelector('.more-sensors-btn-container button');
    if (btn.classList.contains('d-none')){
        btn.classList.toggle('d-none')
    }
    if(sensors.length == 0){
        btn.classList.toggle('d-none')
    }
}

function loadMoreSensors(){
    getLogs(ut, se+=1, 'sensor')
}
function loadMoreUnits(){
    getLogs(ut+=1, se, 'unit')
}