function setNuid(event,usernames) {
    document.querySelector('#nuid').value = event.target.innerHTML;
    var users_container = document.querySelector('#users');
    users_container.value = '';
    for(var i = 0; i<usernames.length; i++){
        users_container.value += usernames[i]+'\n';
    }
}
function search(event) {
    var searched = event.target.value;
    var nuids = document.querySelectorAll("p[id='nuid-container']");
    nuids.forEach(function(nuid,index){
        var div_el = nuid.parentElement;
        if(nuid.innerHTML.indexOf(searched) > -1){
            div_el.style.display = '';
        }else{
            div_el.style.display = 'none';
        }
    })
}
var page = 1;
function getAlarms(pg = 1) {
    page = pg;
    const nuid = document.querySelector('#nuid').value;
    if(!nuid){
        createAlert('','','شناسه شبکه انتخاب شود','danger',true,true,'pageMessages');
    }else{
        axios.post(networkUrl+"?page="+pg+"&nuid="+nuid).then(function (response) {
            if(response.data.length == 0){
                createAlert('','','هشداری یافت نشد','danger',true,true,'pageMessages');
                checkMoreBtnStatus()
            }else{
                fillTable(response.data);
            }

        }).catch(function(error){
            if(error.response != undefined){
                createAlert('','',error.response.data,'danger',true,true,'pageMessages');
            }else{
                console.log(error)
            }
        });
    }
}

function fillTable(alarms) {
    const tbody = document.querySelector('#tbody');
    if (page == 1) {
        tbody.innerHTML = '';
        tbody.innerHTML = alarms;
    } else {
        tbody.insertAdjacentHTML('beforeend',alarms);
    }
    checkMoreBtnStatus(alarms)
}

function checkMoreBtnStatus(alarms = []) {
    let btn = document.querySelector('.more-alarms-btn-container button');
    if (btn.classList.contains('d-none')){
        btn.classList.toggle('d-none')
    }
    if(alarms.length == 0){
        btn.classList.toggle('d-none')
    }
}

function loadMoreAlarms(){
    getAlarms(page+=1)
}