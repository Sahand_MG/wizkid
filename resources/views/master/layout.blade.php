<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>@yield('title') </title>
    <link rel="icon" type="image/x-icon" href="{{asset('favicon.png')}}">
    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="{{asset('font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('dist/css/adminlte.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/animate.css')}}">
    <link rel="stylesheet" href="{{asset('css/alerts.css')}}">
    <link rel="stylesheet" href="{{asset('dist/css/bootstrap-rtl.min.css')}}">
    <link rel="stylesheet" href="{{asset('dist/css/custom-style.css')}}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="{{asset('js/axios.min.js')}}"></script>
    <script src="{{asset('js/alerts.js')}}"></script>
    <script src="{{asset('js/jquery-ui.min.js')}}"></script>
    @yield('scripts')

    {{--<link rel="stylesheet" href="{{asset('css/bootstrap-toggle.min.css')}}">--}}
    {{--<script src="{{asset('js/bootstrap-toggle.min.js')}}"></script>--}}

</head>

<body class="hold-transition sidebar-mini sidebar-collapse">
<div class="wrapper">
    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
            </li>
            <li class="nav-item d-none d-sm-inline-block">
                <a href="{{route('index')}}" class="nav-link">Wizkid</a>
            </li>
        </ul>
        <!-- Right navbar links -->
        <ul class="navbar-nav mr-auto">
            <!-- Messages Dropdown Menu -->

            <!-- Notifications Dropdown Menu -->

        </ul>
    </nav>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <!-- Brand Logo -->
        <a href="{{route('index')}}" class="brand-link">
            {{--<img src="" alt="لوگو سامانه" class="brand-image img-circle elevation-3" style="opacity: .8">--}}
            <span class="brand-text font-weight-light">OWOW</span>
        </a>

        <!-- Sidebar -->
        <div class="sidebar">
            <div>
                <!-- Sidebar user panel (optional) -->
                <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                    <div class="image">
                        <h5 style="color: white">{{user()->name ?? ''}}</h5>
                    </div>
                </div>

                <!-- Sidebar Menu -->
                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                        <!-- Add icons to the links using the .nav-icon class
                             with font-awesome or any other icon font library -->
                        @foreach($sidebar_items as $key => $sidebar_item)

                            @if(!isset($sidebar_item['sub-menu']) && $sidebar_item['show'])
                                <li class="nav-item">
                                    <a href="{{$sidebar_item['href']}}" class="{{$sidebar_item['is_active']?'nav-link active':'nav-link'}}">
                                        <i class="{{$sidebar_item['icon']}}"></i>
                                        <p>{{$sidebar_item['title']}}</p>
                                    </a>
                                </li>
                            @else
                                @if($sidebar_item['show'])
                                <li class="nav-item has-treeview {{$sidebar_item['keep_open']?' menu-open':' menu-close'}}">
                                    <a href="#" class="{{$sidebar_item['parent_active']?'nav-link active':'nav-link'}}">
                                        <i class="{{$sidebar_item['icon']}}"></i>
                                        <p>
                                            {{$sidebar_item['title']}}
                                            <i class="right fa fa-angle-left"></i>
                                        </p>
                                    </a>
                                    <ul class="nav nav-treeview">
                                        @foreach($sidebar_item['sub-menu'] as $name => $submenu)
                                            <li class="nav-item">
                                                <a href="{{$submenu['href']}}" class="{{$submenu['is_active']?'nav-link active':'nav-link'}}">
                                                    <i class="{{$submenu['icon']}}"></i>
                                                    <p style="font-size: 14px">{{$submenu['title']}}</p>
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </li>
                                @endif
                            @endif
                        @endforeach
                    </ul>
                </nav>
                <!-- /.sidebar-menu -->
            </div>
        </div>
        <!-- /.sidebar -->
    </aside>
    @yield('content')

    <footer class="main-footer text-center">
        <!-- Default to the left -->
        <strong class="">CopyLeft &copy; 2020 to Present</strong>
    </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<!-- Bootstrap -->
<script src="{{asset('js/bootstrap.bundle.min.js')}}"></script>
<!-- AdminLTE -->
<script src="{{asset('dist/js/adminlte.js')}}"></script>

<!-- OPTIONAL SCRIPTS -->
{{--<script src="{{asset('plugins/chart.js/Chart.min.js')}}"></script>--}}
{{--<script src="{{asset('dist/js/demo.js')}}"></script>--}}
{{--<script src="{{asset('dist/js/pages/dashboard3.js')}}"></script>--}}
</body>
</html>
