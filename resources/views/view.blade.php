@extends('master.layout')
@section('title')
    Wizkids List
@endsection
@section('scripts')
    <link rel="stylesheet" type="text/css" href="{{asset('assets/DataTables/datatables.css')}}">
    <script type="text/javascript" charset="utf8" src="{{asset('assets/DataTables/datatables.js')}}"></script>
@endsection
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <div id="manege-alert" class="invisible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark"></h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-left">
                            <li class="breadcrumb-item"><a href="{{route('index')}}">Wizkid</a></li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->
    @if(session()->has('error'))
        @include('alerts.session-error')
    @elseif(session()->has('message'))
        @include('alerts.session-success')
    @elseif(count($errors) > 0)
        @include('alerts.form-error')
    @endif
    <!-- Main content -->
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-10 mx-auto">
                        <div class="card">
                            <div class="card-header bg-secondary">
                                <h5 style="display: inline-block;">Search Wizkids</h5>
                            </div>
                            <div class="card-body">
                                <div>
                                    <form class="row"  method="get" action="{{route('search')}} ">
                                        <div class="col-md-3 col-12 mt-2">
                                            <label >Name</label>
                                            <input  type="text" name="name" placeholder="wizkid name" class="form-control">
                                        </div>
                                        <div class="col-md-3 col-12 mt-2">
                                            <label >Role</label>
                                            <select class="form-select" name="role" id="">
                                                <option selected disabled="true">Roles ...</option>
                                                <option value="boss">Boss</option>
                                                <option value="developer">Developer</option>
                                                <option value="designer">Designer</option>
                                                <option value="intern">Intern</option>
                                            </select>
                                        </div>
                                        <div class="row mt-3">
                                            <button class="btn btn-primary col-3" type="submit">Search</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-10 mx-auto">
                        <div class="card">
                            <div class="card-header bg-secondary">
                                <h5 style="display: inline-block;">Wizkids List</h5> <a href="{{route('add')}}" class="btn btn-sm btn-primary">Create Wizkid</a>
                            </div>
                            <div class="card-body">
                                <div class="d-flex">
                                    <table class="table text-center table-hover" style="table-layout:fixed">
                                        <thead>
                                        <tr>
                                            <th>Image</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Phone</th>
                                            <th>Role</th>
                                            <th>Operation</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($users as $user)
                                            <tr>
                                                <td><img width="100" height="100" src="{{$user->picture}}" alt=""></td>
                                                <td>{{$user->name}}</td>
                                                @if(user())
                                                <td>{{$user->email}}</td>
                                                <td>{{$user->phone}}</td>
                                                @else
                                                <td><i class="fa fa-times" style="color: red"></i></td>
                                                <td><i class="fa fa-times" style="color: red"></i></td>
                                                @endif
                                                <td>{{$user->role}}</td>
                                                <td>
                                                    <a href="{{route('edit', ['idx' => hashid($user->id)])}}"><i title="edit" style="color: #0a53be" class="fa fa-edit"></i></a>
                                                    <a href="{{route('delete', ['idx' => hashid($user->id)])}}"><i title="delete" style="color: darkred" class="fa fa-trash"></i></a>
                                                   @if(user())
                                                    @if (is_null($user->deleted_at))
                                                        <a href="{{route('fire', ['idx' => hashid($user->id)])}}"><i title="fire" style="color: darkorange" class="fa fa-times-circle"></i></a>
                                                    @else
                                                        <a href="{{route('unfire', ['idx' => hashid($user->id)])}}"><i title="unfire" style="color: green" class="fa fa-check-circle"></i></a>
                                                    @endif
                                                   @endif
                                                </td>

                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- /.card -->
                        <!-- /.card -->
                    </div>
                    <!-- /.col-md-6 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    {{-- Modal --}}
    <div class="modal fade" id="networkRemoveModal" tabindex="-1" role="dialog" aria-labelledby="networkRemoveModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">تاییدیه</h5>
                    <button type="button" class="close m-1 p-0" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>در صورت اطمینان از حذف شبکه، روی تایید کلیک کنید</p>
                    <input type="hidden" id="network-id">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary mx-2" data-dismiss="modal">بستن پنجره</button>
                    <button type="button" onclick="removeNetwork()" class="btn btn-danger mx-2" data-dismiss="modal">تایید</button>
                </div>
            </div>
        </div>
    </div>
    {{-- ./ Modal --}}
    <div id="pageMessages"></div>
    <!-- Control Sidebar -->

    <!-- /.control-sidebar -->
    <script>
        function setNetwork(nuid) {
            document.querySelector("input[id='network-id']").value = nuid;
        }
        {{--function removeNetwork() {--}}
            {{--var net_id = document.querySelector("input[id='network-id']").value;--}}
            {{--axios.post("{!! route('panel.networks.delete') !!}",{'nuid':net_id}).then(function(response){--}}
                {{--if(response.status == 200){--}}
                    {{--location.reload();--}}
                {{--}--}}
            {{--}).catch(function (error) {--}}
                {{--createAlert('','',' عملیات با خطا مواجه شد ','danger',true,true,'pageMessages');--}}
            {{--})--}}
        {{--}--}}
    </script>
@endsection
<!-- Main Footer -->
