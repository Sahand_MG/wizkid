<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jquerymobile/1.4.5/jquery.mobile.min.css">
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/alerts.css')}}">
    <link rel="stylesheet" href="{{asset('css/animate.css')}}">
    <link rel="stylesheet" href="{{asset('font-awesome/css/font-awesome.min.css')}}">
    <link rel="icon" type="image/x-icon" href="{{asset('favicon.png')}}">
    <script src="{{asset('js/jquery.min.js')}}"></script>
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/alerts.js')}}"></script>
    <title>Login</title>
</head>
<body>
<div>
    @if(session()->has('error'))
        @include('alerts/session-error')
    @elseif(session()->has('message'))
        @include('alerts/session-success')
    @elseif(count($errors) > 0)
        @include('alerts/form-error')
    @endif
</div>
<div class="container">
    <div class="row align-items-center" style="height: 100vh">
        <div class="card col-12 col-sm-6 col-md-4 col-lg-6 mx-auto">
            <div class="card-body">
                <form action="{{route('login')}}" method="post">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input id="email" name="email" type="text" required class="col-12 d-block my-4 p-2" style="border:none; border-bottom: 1px solid #bbbbbb;outline: none" placeholder="Email">
                    <input id="password" name="password" type="password" required class="col-12 d-block my-4 p-2" style="border:none; border-bottom: 1px solid #bbbbbb;outline: none" placeholder="Password">
                    <button class="btn btn-primary my-2">Login</button>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="{{asset('js/bootstrap.bundle.min.js')}}"></script>
</body>
</html>
