<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Fire|Unfire</title>
</head>
<body>
    <div>
        <h2>Hey {{$name}}</h2>
        <p>You have been {{$state}} by admin!</p>
    </div>
</body>
</html>