@extends('master.layout')
@section('content')

    <div class="content-wrapper">
        <div id="manege-alert" class="invisible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h4 class="m-0 text-dark"> Add Wizkid</h4>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-left">
                            <li class="breadcrumb-item"><a href="{{route('index')}}">List</a></li>
                            <li class="breadcrumb-item active"><a href="{{route('add')}}">Add Wizkid</a>
                            </li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->
    @if(session()->has('error'))
        @include('alerts.session-error')
    @elseif(session()->has('message'))
        @include('alerts.session-success')
    @elseif(count($errors) > 0)
        @include('alerts.form-error')
    @endif
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-10 mx-auto">
                        <div class="card">
                            <div class="card-header bg-secondary">
                                <h5 style="display: inline-block">Add Wizkid</h5>
                            </div>
                            <div class="card-body">
                                <form class="row" action="{{route('add')}}" method="post" enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <div class="col-md-3 col-12 mt-2">
                                        <label >Name</label>
                                        <input type="text" name="name" placeholder="name" class="form-control">
                                    </div>
                                    <div class="col-md-3 col-12 mt-2">
                                        <label >Email</label>
                                        <input type="text" name="email" placeholder="email" class="form-control">
                                    </div>
                                    <div class="col-md-3 col-12 mt-2">
                                        <label >Phone</label>
                                        <input type="text" name="phone" placeholder="phone" class="form-control">
                                    </div>
                                    <div class="col-md-3 col-12 mt-2">
                                        <label >Password</label>
                                        <input type="text" name="password" placeholder="password" class="form-control">
                                    </div>

                                    <div class="col-md-3 col-12 mt-2">
                                        <label >Role</label>
                                        <select class="form-select" name="role" id="">
                                            <option selected value="boss">Boss</option>
                                            <option value="developer">Developer</option>
                                            <option value="designer">Designer</option>
                                            <option value="intern">Intern</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3 col-12 mt-2">
                                        <label >Picture</label>
                                        <input type="file" name="picture" placeholder="picture" class="form-control">
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3 col-12 mt-2 ">
                                            <button class="btn btn-primary" type="submit">Submit</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- /.col-md-6 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
    </div>
@endsection